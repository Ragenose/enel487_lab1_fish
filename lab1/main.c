#include <stdio.h>
#include "util.h"
#include "main.h"




int main()
{
	setupRegs();
	int count=0;
	int direction=0;
	//Need to setup the the LED's on the board
  	*regRCC_APB2ENR |= 0x08; // Enable Port B clock
	*regGPIOB_ODR  &= ~0x0000FF00;          /* switch off LEDs                    */
	*regGPIOB_CRH  = 0x33333333; //50MHz  General Purpose output push-pull

  //Writing to this register will turn a light on. Bits 8-15 control the bank of lights
	*regGPIOB_BSRR = MASK_BIT8;  //Set bit 8 
  
  //Writing to the following will turn off a light.
	while(1){
	  	//Forward direction
		if(direction ==0){
			//Set register, turn on led
			*regGPIOB_BSRR = MASK_BIT8<<count;
			delay_ms(200);
			//Reset register, turn off led
			*regGPIOB_BSRR = MASK_BIT24<<count;
			count ++;
		}
		//Revert direction
		if(count == 8){
			count = 1;
			direction = 1;
		}
		if(direction == 1){
			//Set register, turn on led
			*regGPIOB_BSRR = MASK_BIT15>>count;
			delay_ms(200);
			///Reset register, turn off led
			*regGPIOB_BSRR = MASK_BIT31>>count;
			count ++;
		}
		//Revert direction
		if(count == 8){
			count = 1;
			direction =0;
		}
	}
}


