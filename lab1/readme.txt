ENEL 487 Lab1
Zuoxiu Xing 200362635 
Yuming Zhu 200323082

This lab is to create a Cylon by using LEDs. 

In the main function, it initializes all registers and starts Cylon loop.

It will use BSRR register to turn on or turn off LEDs by setting or resetting bits.

It will set and reset register from bit8 to bit15 for turning on LEDs and from bit24 to bit31 for turning off LEDs.

Once the count reaches 8, it will change the direction and it will set and reset register from bit14 to bit8 and from bit30 to bit24.

Once the count reaches 0, it will change the direction again and repeat the above action.