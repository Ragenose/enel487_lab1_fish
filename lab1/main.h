#include "util.h"
#include "registers.h"


//Registers are defined in register.h and register.c
extern volatile uint32_t * regRCC_APB2ENR;	 
extern volatile uint32_t * regGPIOB_ODR;
extern volatile uint32_t * regGPIOB_CRH;
extern volatile uint32_t * regGPIOB_BSRR;
extern volatile uint32_t * regGPIOB_BRR;
  

#define MASK_BIT8  0x00000100
#define MASK_BIT24 0x01000000
#define MASK_BIT15 0x00008100
#define MASK_BIT31 0x80000000